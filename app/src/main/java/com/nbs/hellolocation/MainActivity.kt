package com.nbs.hellolocation

import android.location.Address
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import io.nlopez.smartlocation.OnReverseGeocodingListener
import io.nlopez.smartlocation.SmartLocation
import kotlinx.android.synthetic.main.activity_main.tvLocation
import kotlinx.android.synthetic.main.activity_main.tvReverseLocation

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        SmartLocation.with(this)
            .location()
            .oneFix()
            .start{
                tvLocation.text = "${it.latitude},${it.longitude}"

                SmartLocation.with(this)
                    .geocoding()
                    .reverse(it, object : OnReverseGeocodingListener{
                        override fun onAddressResolved(p0: Location?,
                            p1: MutableList<Address>?) {

                            p1?.let {
                                tvReverseLocation.text =
                                    "${it[0].getAddressLine(0)}"
                            }
                        }
                    })
            }
    }
}
